const EventEmitter = require('events');

class MyEmitter extends EventEmitter { }

const myEmitter = new MyEmitter();

myEmitter.on("newListener", function (eventName, listener) {
    console.log("Added listener for " + eventName + " events");
});


myEmitter.on('greeting', function (name) {
    console.log(`Hey, ${name}`);
});

//myEmitter.emit('greeting', 'Bob');

// var events = require('events');
// var eventEmitter = new events.EventEmitter();
// eventEmitter.on('data_received', function() {
//     console.log('data received succesfully.');
// });

// eventEmitter.emit('data_received'); 


