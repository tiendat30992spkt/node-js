const fs = require('fs');

// const file = fs.createReadStream("Readme.md");

// file.on('data', (chunk) => {
//     console.log(chunk.toString());
// });

// file.on('end', () => {
//     console.log('There will be no data.')
// });

const crypto = require("crypto");
const start = Date.now();

function logHashTime() {
    crypto.pbkdf2("a", "b", 100000, 512, "sha512", () => {
        console.log("Hash: ", Date.now() - start);
    });
}
logHashTime();
logHashTime();
logHashTime();
logHashTime();
logHashTime();

//set UV_THREADPOOL_SIZE=5 & node stream.js