const BootcampModel = require('../models/bootcamps');
// const common = require('../utilities/common');
const asyncHandler = require('../utilities/async');
const errorResponse = require('../utilities/errorResponse')

//@desc     Get all bootcamps
//@route    GET /api/v1/bootcamps
//@access   Public
exports.getBootcamps = async (req, res) => {
    try {
        const bootcamps = await BootcampModel.find();
        res.status(200).send({ success: true, count: bootcamps.length, data: bootcamps });
    } catch (error) {
        next(error)
    }
}

//@desc     Get a bootcamps
//@route    GET /api/v1/bootcamps/:id
//@access   Public
exports.getBootcamp = asyncHandler(async (req, res, next) => {

    const bootcamp = await BootcampModel.findById(req.params.id);
    if (!bootcamp) {
        return next(new errorResponse(`Not found bootcamp with id ${req.params.id}`, 404))
    }
    res.status(200).send({ success: true, data: bootcamp })

})

//@desc     Count all bootcamps
//@route    GET /api/v1/bootcamps/count
//@access   Public
exports.getCountBootcamps = asyncHandler(async (req, res, next) => {

    const bootcamps = await BootcampModel.find();
    res.status(200).send({ success: true, count: bootcamps.length });

})

//@desc     Create a new bootcamps
//@route    POST /api/v1/bootcamps/
//@access   Private
exports.createBootcamp = async (req, res, next) => {
    try {
        const bootcamp = await BootcampModel.create(req.body);
        res.status(201).send({ success: true, data: bootcamp });
    } catch (error) {
        next(error)
    }
}


//@desc     Update a bootcamps
//@route    PUT /api/v1/bootcamps/:id
//@access   Private
exports.updateBootcamp = async (req, res) => {
    try {
        const bootcamp = await BootcampModel.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })
        if (!bootcamp) {
            return next(new errorResponse(`Not found bootcamp with id ${req.params.id}`, 404))
        }
        res.status(200).send({ success: true, data: bootcamp });
    } catch (error) {
        next(error)
    }
}

//@desc     Delete a bootcamps
//@route    DEL /api/v1/bootcamps/:id
//@access   Private
exports.delBootcamp = async (req, res) => {
    try {
        const bootcamp = await BootcampModel.findByIdAndRemove(req.params.id)
        if (!bootcamp) {
            return next(new errorResponse(`Not found bootcamp with id ${req.params.id}`, 404))
        }
        res.status(200).send({ success: true });
    } catch (error) {
        next(error)
    }
}