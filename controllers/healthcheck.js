const asyncHandler = require('../utilities/async');
const bootcampController = require('./bootcamps')

//@desc     K8S check liveness
//@route    GET /liveness
//@access   Public
exports.liveness = asyncHandler(async (req, res, next) => {
    res.status(200).send({ success: true, message: 'App is running' })
})

//@desc     K8S check readiness
//@route    GET /readiness
//@access   Public
exports.readiness = bootcampController.getCountBootcamps