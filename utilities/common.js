module.exports.log = function (msg) {
    console.log(msg);
};

const errorHandler = (err, req, res, next) => {
    //Mongo Duplicate
    if (err.name === 'MongoError' && err.code === 11000) {
        err.statusCode = 400;
    }
    if (err.name === 'ValidationError') {
        err.statusCode = 400;
    }
    if (err.name === 'CastError') {
        err.statusCode = 400;
    }
    this.log(err);
    res.status(err.statusCode || 500).json({ success: false, msg: err.message || 'Server have problem' });
}

module.exports.errorHandler = errorHandler;

