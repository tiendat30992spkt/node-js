const NodeGeocoder = require('node-geocoder')

const geocoder = () => {
    const options = {
        provider: process.env.GEOCODER_PROVIDER,
        apiKey: process.env.GEOCODER_API_KEY,
        formatter: null
    };
    return NodeGeocoder(options);
}

module.exports = geocoder