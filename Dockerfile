FROM node:latest

# author
MAINTAINER Nguyen Quang Nguyen

# extra metadata
LABEL version="1.0"
LABEL description="NodeJS image with Dockerfile."

# Update Ubuntu Software repository
RUN apt-get update

#copy soure to app
COPY ./ /app/
RUN cd /app && \
    npm install
    

WORKDIR /app

ENTRYPOINT ["npm"]

CMD ["test"]

EXPOSE 80