const mongoose = require('mongoose');

const connectDB = async () => {
    // console.log(`MongoDB URI: ${process.env.MONGO_URI}`)
    const conn = await mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    });
    // console.log(`MongoDB connected: ${process.env.MONGO_URI}`)
}

module.exports = connectDB;