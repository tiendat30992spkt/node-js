const http = require('http');

// const server = http.createServer();
// body = '';
// count = 0;
// server.on('request', (req, res) => {

//     req.on('data', function (chunk) {
//         console.log(chunk);
//         body += chunk;
//         count++;
//     });
//     req.on('end', function () {
//         //console.log(`end ${body}`);
//         console.log(`end ${count}`);
//     })
//     res.end('Hello World!');
// });

// server.listen(3000);



const hostname = '127.0.0.1'
const port = 3000;

const server = http.createServer((req, res) => {
    req.pipe(res);
})

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
})


