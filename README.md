# NodeJS-Express

## CI/CD

### CI
- Build a private Gitlab Runner on EC2 with mode docker excutor 
- Build a docker image base on a dockerfile
- Push the image to Docker Hub


### CD
- Deploy minikube on a EC2 with mode NodePort
- Run the Deployment and Service yaml
