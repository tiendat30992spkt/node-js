const express = require('express');
const dotenv = require('dotenv');
const bootcampRouter = require('./routes/bootcamps');
const healthRouter = require('./routes/healthcheck');
const morgan = require('morgan')
const common = require('./utilities/common')
const connectDB = require('./config/db')
const fs = require('fs');

dotenv.config({ path: './config/config.env' })

const app = express();
const PORT = process.env.PORT || 3000;

//Connect DB
connectDB();

//body parser
app.use(express.json());

//Logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// app.use('/', function (req, res, next) {
//     console.log(`${req.method} ${req.url}`);
//     next();
// })

// app.get('/api/v1/bootcamps', (req, res) => {
//     res.status(200).send({ success: true, data: { id: 1 } });
// });
// app.post('/api/v1/bootcamps', (req, res) => {
//     res.status(200).send({ success: true, data: { id: 1 } });
// });
// app.put('/api/v1/bootcamps/:id', (req, res) => {
//     res.status(200).send({ success: true, data: { id: `${req.params.id}` } });
// });
// app.delete(''); ('/api/v1/bootcamps/:id', (req, res) => {
//     res.status(200).send({ success: true, data: { id: `${req.params.id}` } });
// });

// Routing
app.use('/api/v1/bootcamps', bootcampRouter);
app.use('/api/', healthRouter);

//Middleware
app.use(common.errorHandler);

//Start Server
const server = app.listen(PORT, () => {
    console.log(`App listening on ${process.env.NODE_ENV} port ${PORT}`);
    fs.appendFile('persistentDisk\message.txt', 'data to append\n', function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
});

process.on('unhandledRejection', (reason, promise) => {
    // unhandledRejections.set(promise, reason);
    console.log(`Error: ${reason}`);
    server.close(() => process.exit(1))
});