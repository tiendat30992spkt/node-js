const express = require('express');
const router = express.Router();

const { liveness, readiness } = require('../controllers/healthcheck')

router.route('/liveness')
    .get(liveness)

router.route('/readiness')
    .get(readiness)

module.exports = router;