const express = require('express');
const router = express.Router();

const { getBootcamps, getBootcamp, createBootcamp, updateBootcamp, delBootcamp } = require('../controllers/bootcamps')

// router.get('/', (req, res) => {
//     res.status(200).send({ success: true, data: 'Show all bootcamps' });
// });
// router.post('/', (req, res) => {
//     res.status(200).send({ success: true, data: { id: 1 } });
// });
// router.put('/:id', (req, res) => {
//     res.status(200).send({ success: true, data: { id: `${req.params.id}` } });
// });
// router.delete(''); ('/:id', (req, res) => {
//     res.status(200).send({ success: true, data: { id: `${req.params.id}` } });
// });

router.route('/')
    .get(getBootcamps)
    .post(createBootcamp);

router.route('/:id')
    .get(getBootcamp)
    .put(updateBootcamp)
    .delete(delBootcamp)

module.exports = router;
